<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo e(ucfirst(Request::segment(2))); ?>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo e(ucfirst(Request::segment(2))); ?></a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Concoeur Reports</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Email</th>
                        <th>Date Sent</th>
                        <th>Option</th>
						
                    </tr>
                </thead>
				 <tbody>
                    <?php $x=1;?>
                    <?php $__empty_1 = true; $__currentLoopData = $reports; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $report): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e($x++); ?></td>
                        <td><?php echo e($report->title); ?></td>
                        <td><?php echo e(($report->body)); ?></td>
                        <td><?php echo e($report->email); ?></td>
                        <td><?php echo e($report->created_at); ?></td>
                      
						
                        <td>
                        <a href="mailto:<?php echo e($report->email); ?>" data-toggle="tooltip" title="Reply" class="btn btn-primary"> 
						
						<i class="fa fa-edit"></i>Reply</a>
                        </td>
                    </tr>
					</tbody>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr><td colspan="4"><center class="alert alert-danger">No Reports to display :(</center></td></tr>
                    <?php endif; ?>
            </table>
            <?php if(Request::segment(2)=="reports"): ?>
            <center><?php echo e($reports->links()); ?></center>
            <?php endif; ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>