<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrators
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Administrators</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Administrators</h3>

          <div class="box-tools pull-right">
            <a href="<?php echo e(url('admin/user/create')); ?>" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new User</a>
          </div>
        </div>
        <div class="box-body table-responsive">
            
            <?php if(session('message')): ?>
            
            <center class="alert alert-success"><?php echo e(session('message')); ?></center>
            
            <?php endif; ?>
            
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Date Created</th>
                       <th>
					   <div class="pull-right">
						Options    &emsp;&emsp;&emsp;
				       </div>
					   </th>
                    </tr>
                </thead>
				
				<tbody>
                    <?php $x=1;?>
                    <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e($x++); ?></td>
                        <td><?php echo e($user->name); ?></td>
                        <td><?php echo e($user->email); ?></td>
                        <td><?php echo e(DB::table('admin_roles')->where('id',$user->role)->value('title')); ?></td>
                        <td><?php echo e($user->created_at); ?></td>
						
					
                        <td>
						<div class="pull-right">
                            <a href="<?php echo e(url('/admin/user/edit')); ?>/<?php echo e($user->id); ?>" class="btn btn-primary btn-inline"><i class="fa fa-edit"></i> Edit</a>
                           <a href="<?php echo e(url('/admin/user/delete')); ?>/<?php echo e($user->id); ?>" onclick="return confirm('Are You Sure You want to Delete this?')" class="btn btn-danger btn-inline"><i class="fa fa-trash"></i> Delete</a>
                       </div>
					   </td>
				
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr><td colspan="8"><center class="alert alert-danger">No User to display :(</center></td></tr>
                    <?php endif; ?>
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <?php echo e($users->links()); ?>

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>