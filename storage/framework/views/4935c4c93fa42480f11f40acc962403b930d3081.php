<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'ConcoeurOnline')); ?></title>
	
    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
		
	
    <?php echo $__env->yieldContent('css'); ?>
    
	  <link href="<?php echo e(asset('css/style.css')); ?>" rel="stylesheet">
	  
	<?php if(Request::is( Config::get('chatter.routes.home') ) || Request::is( Config::get('chatter.routes.home') . '/*' )): ?>
    <!-- LINK TO YOUR CUSTOM STYLESHEET -->
   
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="<?php echo e(asset('css/style-navy.css')); ?>">
	 <!-- Theme Styles -->
	  <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
	  	  <!-- Theme Styles -->
	  <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
     <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dosis:400,300,500,600,700' rel='stylesheet' type='text/css'>
	
    <!-- Updated Styles -->
    <link rel="stylesheet" href="<?php echo e(asset('css/updates.css')); ?>">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="<?php echo e(asset('css/responsive.css')); ?>">


	  
<?php endif; ?>
	
	
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        <?php echo e(config('app.name', 'ConcoeurOnline')); ?>

                    </a>
                </div>

                <div>
                    <form method="post" action="<?php echo e(route('chatter.search')); ?>" class = "navbar-form navbar-left" role = "search">
                        <?php echo e(csrf_field()); ?>

                        <div class = "form-group">
                            <input type = "search" name="q" class = "form-control" placeholder = "Discussions Title">
                        </div>
                        <button type = "submit" class="btn btn-primary">Search</button>

                    </form>
                </div>


                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <?php if(auth()->guard()->guest()): ?>
                            <li><a href="<?php echo e(route('login')); ?>"><i style="margin-right: 3px" class="glyphicon glyphicon-log-in" aria-hidden="true"></i>Login</a></li>
                            <li><a href="<?php echo e(route('register')); ?>"><i style="margin-right: 3px" class="glyphicon glyphicon-edit" aria-hidden="true"></i>Register</a></li>
                        <?php else: ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <i style="margin-right: 3px" class="glyphicon glyphicon-user" aria-hidden="true"></i><?php echo e(Auth::user()->name); ?><span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo e(route('logout')); ?>"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <!-- Scripts -->
    <script src="<?php echo e(asset('js/app.js')); ?>"></script>
    <?php echo $__env->yieldContent('js'); ?>

    <script src="<?php echo e(asset('js/trumbowyg.upload.min.js')); ?>"></script>
</body>
</html>
