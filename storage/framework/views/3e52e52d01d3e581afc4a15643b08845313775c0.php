<!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo e(asset('dist/img/user2-160x160.jpg')); ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">

          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>

	  
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
	  
        <li class="header">MAIN NAVIGATION</li>
       
	   <li><a href="<?php echo e(url('/admin/dashboard')); ?>"><i class="fa fa-desktop"></i> <span>Dashboard</span></a></li>
       
	   <li><a href="<?php echo e(url('/admin/announcement')); ?>"><i class="fa fa-users"></i> <span>Announcement</span></a></li>
       
	   <li><a href="<?php echo e(url('/admin/advertisement')); ?>"><i class="fa fa-newspaper-o"></i> <span>Advertisement</span></a></li>
       
	   <li><a href="<?php echo e(url('/admin/report')); ?>" ><i class="fa fa-folder-open"></i> <span >Reports</span></a>
		
		<?php if(Auth::user()->role ==1): ?>
	   <li><a href="<?php echo e(url('/admin/user')); ?>"><i class="fa fa-user"></i>Users</a></li>
        <?php endif; ?>
	 


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>