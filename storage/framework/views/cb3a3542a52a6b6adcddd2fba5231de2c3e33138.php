<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo e(ucfirst(Request::segment(2))); ?>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo e(ucfirst(Request::segment(2))); ?></a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Concoeur Announcements</h3>

          <div class="box-tools pull-right">
		   <a href="<?php echo e(url('admin/announcement/create')); ?>" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new Announcements</a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive" style="overflow-x:auto;">
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Date Posted</th>
                        <th>Options</th>
						
                    </tr>
                </thead>
				 <tbody>
                    <?php $x=1;?>
                    <?php $__empty_1 = true; $__currentLoopData = $announce; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e($x++); ?></td>
                        <td><?php echo e($article->title); ?></td>
                        <td><?php echo nl2br($article->body); ?></td>
                        <td><?php echo e($article->created_at); ?></td>
                        <td>
						
                        <a href="<?php echo e(url('/admin/announcement/edit')); ?>/<?php echo e($article->id); ?>" data-toggle="tooltip" title="Edit" class="btn btn-primary"> 
						<i class="fa fa-edit"></i>Update</a><br>
						
							
                        <a href="<?php echo e(url('/admin/announcement/delete')); ?>/<?php echo e($article->id); ?>" class="btn btn-danger btn-inline" onclick="return confirm('are you sure you want to delete this?')" ><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr><td colspan="4"><center class="alert alert-danger">No Announcements to display :(</center></td></tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <?php if(Request::segment(2)=="announcements"): ?>
            <center><?php echo e($announce->links()); ?></center>
            <?php endif; ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>