<?php $__env->startSection(Config::get('chatter.yields.head')); ?>
    <link href="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css" rel="stylesheet">
	<link href="/vendor/devdojo/chatter/assets/css/chatter.css" rel="stylesheet">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div id="chatter" class="chatter_home">
<!--Banner logo-->
	<div id="chatter_hero">
		<div id="chatter_hero_dimmer"></div>
		
		<?php $headline_logo = Config::get('chatter.headline_logo'); ?>
		
		<?php if( isset( $headline_logo ) && !empty( $headline_logo ) ): ?>
			
			<img src="<?php echo e(Config::get('chatter.headline_logo')); ?>">
			
		<?php else: ?>
			
			<h1><?php echo e(Config::get('chatter.headline')); ?></h1>
			<p><?php echo e(Config::get('chatter.description')); ?></p>
		<?php endif; ?>
	</div>
<!--END Banner-->


	<?php if(Session::has('chatter_alert')): ?>
		<div class="chatter-alert alert alert-<?php echo e(Session::get('chatter_alert_type')); ?>">
			<div class="container">
	        	<strong><i class="chatter-alert-<?php echo e(Session::get('chatter_alert_type')); ?>"></i> <?php echo e(Config::get('chatter.alert_messages.' . Session::get('chatter_alert_type'))); ?></strong>
	        	<?php echo e(Session::get('chatter_alert')); ?>

	        	<i class="chatter-close"></i>
	        </div>
	    </div>
	    <div class="chatter-alert-spacer"></div>
	<?php endif; ?>

	<?php if(count($errors) > 0): ?>
	    <div class="chatter-alert alert alert-danger">
	    	<div class="container">
	    		<p><strong><i class="chatter-alert-danger"></i> <?php echo e(Config::get('chatter.alert_messages.danger')); ?></strong> Please fix the following errors:</p>
		        <ul>
		            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                <li><?php echo e($error); ?></li>
		            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		        </ul>
		    </div>
	    </div>
	<?php endif; ?>

	<div class="container chatter_container">

	    <div class="row">

	    	<div class="col-md-3 left-column">
	    		<!-- SIDEBAR -->
	    		<div class="chatter_sidebar">
				
				<a href="<?php echo e(url('/')); ?>" ><span class='glyphicon glyphicon-home'></span> Home</a>
					
				</div>
				<!-- END SIDEBAR -->
			</div>
	    
	        <div  class="col-md-9 right-column">
			<h1 style="padding:5px; background:#fafafa; text-align:center;">Concoeuronline Report Form</h1>
	        	
				 <?php if(session('message')): ?>
                        <center class='alert alert-success'><?php echo session('message'); ?></center>
                        <?php endif; ?>
                        <form method="POST" action="<?php echo e(url('/forums/report')); ?>" class="form-horizontal" style="padding:5px; margin:5px; background:#fafafa;">
                            <?php echo e(csrf_field()); ?>

                          
							
							<div class="form-group">
							<label class="control-label col-md-3" for="email">Email:</label>
						
						   <div class="col-md-6">
						
                            <input type="email" name="email" class="form-control" placeholder="Email">
                            </div>
                            </div>
                         
							<div class="form-group">
							<label class="control-label col-md-3" for="title">Report Title:</label>
							
							<div class="col-md-6">
                                <input type="text" name="title" class="form-control" placeholder="Report Title">
                            </div>
                            </div>
                           
						   <div class="form-group">
						   <label class="control-label col-md-3" for="content">Report Description:</label>
						   
						   <div class="col-md-6">
                                <textarea  type="text" class="form-control" name="body" placeholder="Report Description" rows="2"></textarea>
                            </div>
                            </div>
                         
                          
                            <!--the end-->
                            <div class="form-group">
							  <div class="col-md-6 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            </div>
                        </form>

	        </div>
	    </div>
	</div>

	

<?php $__env->stopSection(); ?>

<?php echo $__env->make(Config::get('chatter.master_file_extend'), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>