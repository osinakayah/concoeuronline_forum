<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Concoeuronline Announcement Board
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-info-circle"></i>  Concoeuronline Announcement Board</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Update Concoeuronline Announcement Board</h3>

          <div class="box-tools pull-right">
            
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-offset-1 col-md-7">
                    
                    <?php if(session('message')): ?>
            
                    <center class="alert alert-success"><?php echo e(session('message')); ?></center>

                    <?php endif; ?>
                    
                    <form class="form-horizontal" method="POST" action="<?php echo e(url('admin/advertisement/edit/')); ?>/<?php echo e($advert->id); ?>" enctype="multipart/form-data" >
                        <?php echo e(csrf_field()); ?>

                        
                        <input name="id" value="<?php echo e($advert->id); ?>" type="hidden"/>
						
                        <div class="form-group">
                            <label class="control-label col-md-3" for="title">Advertisement Title:</label>
                            
                            <div class="col-md-8">
                                <input name="advert_name" value="<?php echo e($advert->advert_name); ?>" type="text" class="form-control" maxlength="190" 
								placeholder="Advertisement Title" required/>
                            </div>
                        </div>
                        
						<div class="form-group">
                            <label class="control-label col-md-3" for="title">Advertisement Image:</label>
                            
                            <div class="col-md-8">
                                <input name="advert_image" type="file" class="form-control"  >
                            </div>
                            
                        </div>
					
						
                        
                        <div class="col-md-offset-3"><button type="submit" class="btn btn-success">Save</button></div>
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>