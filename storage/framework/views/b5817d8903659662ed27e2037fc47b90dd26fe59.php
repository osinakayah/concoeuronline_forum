<?php $__env->startSection('content'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo e(ucfirst(Request::segment(2))); ?>

      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> <?php echo e(ucfirst(Request::segment(2))); ?></a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Concoeur Advertisements</h3>

          <div class="box-tools pull-right">
		   <a href="<?php echo e(url('admin/advertisement/create')); ?>" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new Advert</a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Advertiser Name</th>
                        <th>Advertisement Image</th>
                        <th>
						<div class="pull-right">
						Options    &emsp;&emsp;&emsp;&emsp;
				       <div>
					
						</th>
						
						
                    </tr>
                </thead>
				 <tbody>
                    <?php $x=1;?>
                    <?php $__empty_1 = true; $__currentLoopData = $advert; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $article): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <tr>
                        <td><?php echo e($x++); ?></td>
                        <td><?php echo e($article->advert_name); ?></td>
                        <td>
						
						<?php if($article->advert_image != null): ?>
                            <?php
                                $arr = explode('/', $article->advert_image);
                                $article->advert_image = "/".$arr[1]."/".$arr[2];
                            ?>
                            <img src="<?php echo e(asset($article->advert_image)); ?>" style="width:100px; height:150px;" class="img-responsive">
                            <?php endif; ?>
						
						</td>
                        <td>
						<div class="pull-right">
                        <a href="<?php echo e(url('/admin/advertisement/edit')); ?>/<?php echo e($article->id); ?>" data-toggle="tooltip" title="Edit" class="btn btn-primary"> 
						<i class="fa fa-edit"></i>Update</a>
							
                            <a href="<?php echo e(url('/admin/advertisement/delete')); ?>/<?php echo e($article->id); ?>" class="btn btn-danger btn-inline" onclick="return confirm('are you sure you want to delete this?')" ><i class="fa fa-trash"></i> Delete</a>
                       </div>
					   </td>
                    </tr>
					 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <tr><td colspan="4"><center class="alert alert-danger">No Advertisement to display :(</center></td></tr>
                    <?php endif; ?>
                </tbody>
                
            </table>
            <?php if(Request::segment(2)=="advertisements"): ?>
            <center><?php echo e($advert->links()); ?></center>
            <?php endif; ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>