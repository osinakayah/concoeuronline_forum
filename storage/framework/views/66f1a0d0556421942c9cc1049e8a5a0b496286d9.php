<?php $__env->startSection(Config::get('chatter.yields.head')); ?>
    <link href="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css" rel="stylesheet">
	<link href="/vendor/devdojo/chatter/assets/css/chatter.css" rel="stylesheet">

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<div id="chatter" class="chatter_home">
<!--Banner logo-->
	<div id="chatter_hero">
		<div id="chatter_hero_dimmer"></div>
		
		<?php $headline_logo = Config::get('chatter.headline_logo'); ?>
		
		<?php if( isset( $headline_logo ) && !empty( $headline_logo ) ): ?>
			
			<img src="<?php echo e(Config::get('chatter.headline_logo')); ?>">
			
		<?php else: ?>
			
			<h1><?php echo e(Config::get('chatter.headline')); ?></h1>
			<p><?php echo e(Config::get('chatter.description')); ?></p>
		<?php endif; ?>
	</div>
<!--END Banner-->


	<?php if(Session::has('chatter_alert')): ?>
		<div class="chatter-alert alert alert-<?php echo e(Session::get('chatter_alert_type')); ?>">
			<div class="container">
	        	<strong><i class="chatter-alert-<?php echo e(Session::get('chatter_alert_type')); ?>"></i> <?php echo e(Config::get('chatter.alert_messages.' . Session::get('chatter_alert_type'))); ?></strong>
	        	<?php echo e(Session::get('chatter_alert')); ?>

	        	<i class="chatter-close"></i>
	        </div>
	    </div>
	    <div class="chatter-alert-spacer"></div>
	<?php endif; ?>

	<?php if(count($errors) > 0): ?>
	    <div class="chatter-alert alert alert-danger">
	    	<div class="container">
	    		<p><strong><i class="chatter-alert-danger"></i> <?php echo e(Config::get('chatter.alert_messages.danger')); ?></strong> Please fix the following errors:</p>
		        <ul>
		            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		                <li><?php echo e($error); ?></li>
		            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		        </ul>
		    </div>
	    </div>
	<?php endif; ?>

	<div class="container chatter_container">

	    <div class="row">

	    	<div class="col-md-3 left-column">
	    		<!-- SIDEBAR -->
	    		<div class="chatter_sidebar">
				
				<a href="<?php echo e(url('/')); ?>" ><span class='glyphicon glyphicon-home'></span> Home</a>
					
				</div>
				<!-- END SIDEBAR -->
			</div>
	    
	        <div class="col-md-9 right-column">
			<h1 style="padding:5px; background:#fafafa; text-align:center;">Concoeuronline Announcement Board</h1>
	        <div class="panel">
				<?php $__empty_1 = true; $__currentLoopData = $announcement; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $announce): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
		        	<ul class="discussions" style="padding:5px; margin:5px; background:#fafafa; text-align:justify;">
		        			<div class="panel">
				        	<li style="padding:5px">
							
							<div class="chatter_middle">
				        	<h3 class="chatter_middle_title"><?php echo e($announce->title); ?></h3>	
				        	<p><?php echo e($announce->body); ?></p>	
				        	<p><span><?php echo e(\Carbon\Carbon::createFromTimeStamp(strtotime($announce->created_at))->diffForHumans()); ?></span></p>	

							</div>
					        </li>	
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <p colspan="4"><center class="alert alert-danger">No Announcements to display :(</center></p>						
			        </div>	
		        	</ul>
	        	</div>
			<?php endif; ?>
	        	<div id="pagination">
	        		<?php echo e($announcement->links()); ?>

	        	</div>

	        </div>
	    </div>
	</div>

	

<?php $__env->stopSection(); ?>

<?php echo $__env->make(Config::get('chatter.master_file_extend'), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>