@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Administrators
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Administrators</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Administrators</h3>

          <div class="box-tools pull-right">
            <a href="{{url('admin/user/create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new User</a>
          </div>
        </div>
        <div class="box-body table-responsive">
            
            @if(session('message'))
            
            <center class="alert alert-success">{{session('message')}}</center>
            
            @endif
            
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Date Created</th>
                       <th>
					   <div class="pull-right">
						Options    &emsp;&emsp;&emsp;
				       </div>
					   </th>
                    </tr>
                </thead>
				
				<tbody>
                    <?php $x=1;?>
                    @forelse($users as $user)
                    <tr>
                        <td>{{$x++}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{DB::table('admin_roles')->where('id',$user->role)->value('title')}}</td>
                        <td>{{$user->created_at}}</td>
						
					
                        <td>
						<div class="pull-right">
                            <a href="{{url('/admin/user/edit')}}/{{$user->id}}" class="btn btn-primary btn-inline"><i class="fa fa-edit"></i> Edit</a>
                           <a href="{{url('/admin/user/delete')}}/{{$user->id}}" onclick="return confirm('Are You Sure You want to Delete this?')" class="btn btn-danger btn-inline"><i class="fa fa-trash"></i> Delete</a>
                       </div>
					   </td>
				
                    </tr>
                    @empty
                    <tr><td colspan="8"><center class="alert alert-danger">No User to display :(</center></td></tr>
                    @endforelse
                </tbody>

            </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$users->links()}}
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection