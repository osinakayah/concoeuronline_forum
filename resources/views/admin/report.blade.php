@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst(Request::segment(2))}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ucfirst(Request::segment(2))}}</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Concoeur Reports</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Email</th>
                        <th>Date Sent</th>
                        <th>Option</th>
						
                    </tr>
                </thead>
				 <tbody>
                    <?php $x=1;?>
                    @forelse($reports as $report)
                    <tr>
                        <td>{{$x++}}</td>
                        <td>{{$report->title}}</td>
                        <td>{{($report->body)}}</td>
                        <td>{{$report->email}}</td>
                        <td>{{$report->created_at}}</td>
                      
						
                        <td>
                        <a href="mailto:{{$report->email}}" data-toggle="tooltip" title="Reply" class="btn btn-primary"> 
						
						<i class="fa fa-edit"></i>Reply</a>
                        </td>
                    </tr>
					</tbody>
                  @empty
                    <tr><td colspan="4"><center class="alert alert-danger">No Reports to display :(</center></td></tr>
                    @endforelse
            </table>
            @if(Request::segment(2)=="reports")
            <center>{{$reports->links()}}</center>
            @endif
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection