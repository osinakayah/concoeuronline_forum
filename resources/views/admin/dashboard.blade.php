@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst(Request::segment(2))}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ucfirst(Request::segment(2))}}</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Recent Activities</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Discussion  Title</th> 
                        <th>Category</th>
                        <th>Date Created</th>
						<th>Posted By</th>

                    </tr>
                </thead>
                 <tbody>
                    <?php $x=1;?>
                    @forelse($discussions as $discussion)
                    <tr>
					@foreach($categories as $category)
					  @if($category->id == $discussion->chatter_category_id)
                        <td>{{$x++}}</td>
                        <td><a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}/{{ $category->slug }}/{{$discussion->slug}}">{{$discussion->title}}</a></td>

                                  
                        <td>{{$category->name}}</td>
					
					@endif
                              @endforeach
									
							
                            
						
						 
						
                              
							  
							<td>{{$discussion->created_at}}</td>  
							
                      @foreach($users as $user)
                            
							@if($user->id == $discussion->user_id)

                        <td>{{$user->name}}</td>
						@endif
                              @endforeach
                    </tr>
                    @empty
                    <tr><td colspan="7"><center class="alert alert-danger">No Recent Discussion to display :(</center></td></tr>
                    @endforelse
                </tbody>
            </table>
            @if(Request::segment(2)=="applications")
            <center>{{$applications->links()}}</center>
            @endif
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection