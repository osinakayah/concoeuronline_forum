@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ucfirst(Request::segment(2))}}
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ucfirst(Request::segment(2))}}</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Concoeur Announcements</h3>

          <div class="box-tools pull-right">
		   <a href="{{url('admin/announcement/create')}}" class="btn btn-default"><i class="fa fa-plus-circle"></i> Add new Announcements</a>
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body table-responsive" style="overflow-x:auto;">
            <table class="table">
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th>Date Posted</th>
                        <th>Options</th>
						
                    </tr>
                </thead>
				 <tbody>
                    <?php $x=1;?>
                    @forelse($announce as $article)
                    <tr>
                        <td>{{$x++}}</td>
                        <td>{{$article->title}}</td>
                        <td>{!!nl2br($article->body)!!}</td>
                        <td>{{$article->created_at}}</td>
                        <td>
						
                        <a href="{{url('/admin/announcement/edit')}}/{{$article->id}}" data-toggle="tooltip" title="Edit" class="btn btn-primary"> 
						<i class="fa fa-edit"></i>Update</a><br>
						
							
                        <a href="{{url('/admin/announcement/delete')}}/{{$article->id}}" class="btn btn-danger btn-inline" onclick="return confirm('are you sure you want to delete this?')" ><i class="fa fa-trash"></i> Delete</a>
                        </td>
                    </tr>
					 @empty
                    <tr><td colspan="4"><center class="alert alert-danger">No Announcements to display :(</center></td></tr>
                    @endforelse
                </tbody>
            </table>
            @if(Request::segment(2)=="announcements")
            <center>{{$announce->links()}}</center>
            @endif
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection