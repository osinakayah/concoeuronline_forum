@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Concoeuronline Announcement Board
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-info-circle"></i>  Concoeuronline Announcement Board</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Update Concoeuronline Announcement Board</h3>

          <div class="box-tools pull-right">
            
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-offset-1 col-md-7">
                    
                    @if(session('message'))
            
                    <center class="alert alert-success">{{session('message')}}</center>

                    @endif
                    
                    <form class="form-horizontal" method="POST" action="{{url('admin/announcement/edit/')}}/{{$announce->id}}" >
                        {{csrf_field()}}
                        
                        <input name="id" value="{{$announce->id}}" type="hidden"/>
						
                        <div class="form-group">
                            <label class="control-label col-md-3" for="title">Announcement Title:</label>
                            
                            <div class="col-md-8">
                                <input name="title" value="{{$announce->title}}" type="text" class="form-control" maxlength="190" 
								placeholder="Announcement Title" required/>
                            </div>
                        </div>
                        
						
						<div class="form-group">
                            <label class="control-label col-md-3" for="body">Announcement Content:</label>
                            
                            <div class="col-md-8">
                                <textarea name="body" type="text-area" class="form-control"  rows="4">{{$announce->body}}</textarea>
                            </div>
                            
                        </div>
						
                        
                        <div class="col-md-offset-3"><button type="submit" class="btn btn-success">Save</button></div>
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection