@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
          Administrators
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-info-circle"></i> Administrators</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Update Administrators Details</h3>

          <div class="box-tools pull-right">
            
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-offset-1 col-md-7">
                    
                    @if(session('message'))
            
                    <center class="alert alert-success">{{session('message')}}</center>

                    @endif
                    
                    <form class="form-horizontal" method="POST" action="{{url('admin/user/edit/')}}/{{$user->id}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        
                        <input name="id" value="{{$user->id}}" type="hidden"/>
                         <div class="form-group">
                            <label class="control-label col-md-3" for="title">Admin  Name:</label>
                            
                            <div class="col-md-8">
                                <input name="name" type="text" class="form-control" maxlength="45" placeholder="Name" value="{{$user->name}}"/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="url">Admin Email:</label>
                            
                            <div class="col-md-8">
                                <input name="email" placeholder="someone@example.com" class="form-control" type="email" value="{{$user->email}}"/>
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="phone">Admin Password:</label>
                            
                            <div class="col-md-8">
                                <input  value="{{$user->password}}" name="password" placeholder="Password(Minimum of 6 Characters)" class="form-control" type="text"  readonly />
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="phone">New Password:</label>
                            
                            <div class="col-md-8">
                                <input name="password" placeholder="New Password" class="form-control" type="password" />
                            </div>
                            
                        </div>
                        
						<div class="form-group">
                            <label class="control-label col-md-3" for="phone">Confirm Password:</label>
                            
                            <div class="col-md-8">
                                <input name="confirm_password" placeholder="Confirm Password" class="form-control" type="password" />
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="title">Admin Role:</label>
                            
                            <div class="col-md-8">
                                <select name="role" class="form-control" required>
                                    <option value="">Select Admin Role</option>
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                       
                        
                        
                        <div class="col-md-offset-3"><button type="submit" class="btn btn-success">Save</button></div>
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
@endsection