@extends('layouts.admin.app')


@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Users</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Add User</h3>

          <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-offset-1 col-md-7">
                    
                    @if(session('message'))
            
                    <center class="alert alert-success">{!!session('message')!!}</center>

                    @endif
                    
                    @if(session('error'))
            
                    <center class="alert alert-danger">{!!session('error')!!}</center>

                    @endif
                    
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{url('/admin/user/create')}}">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="control-label col-md-3" for="title">Admin  Name:</label>
                            
                            <div class="col-md-8">
                                <input name="name" type="text" class="form-control" maxlength="45" placeholder="Name" required/>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="url">Admin Email:</label>
                            
                            <div class="col-md-8">
                                <input name="email" placeholder="someone@example.com" class="form-control" type="email" required/>
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="phone">Admin Password:</label>
                            
                            <div class="col-md-8">
                                <input name="password" placeholder="Password(Minimum of 6 Characters)" class="form-control" type="password" min="6" required/>
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="phone">Confirm Password:</label>
                            
                            <div class="col-md-8">
                                <input name="confirm_password" placeholder="Confirm Password" class="form-control" type="password" required/>
                            </div>
                            
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-3" for="title">Admin Role:</label>
                            
                            <div class="col-md-8">
                                <select name="role" class="form-control" required>
                                    <option value="">Select Admin Role</option>
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                        </div>
                        
                        <div class="col-md-offset-3"><button type="submit" class="btn btn-success">Create Admin</button></div>
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
        


    </section>
    <!-- /.content -->
  </div>
@endsection