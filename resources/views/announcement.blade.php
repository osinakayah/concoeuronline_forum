@extends('layouts.app')

@extends(Config::get('chatter.master_file_extend'))

@section(Config::get('chatter.yields.head'))
    <link href="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css" rel="stylesheet">
	<link href="/vendor/devdojo/chatter/assets/css/chatter.css" rel="stylesheet">

@stop

@section('content')

<div id="chatter" class="chatter_home">
<!--Banner logo-->
	<div id="chatter_hero">
		<div id="chatter_hero_dimmer"></div>
		
		<?php $headline_logo = Config::get('chatter.headline_logo'); ?>
		
		@if( isset( $headline_logo ) && !empty( $headline_logo ) )
			
			<img src="{{ Config::get('chatter.headline_logo') }}">
			
		@else
			
			<h1>{{ Config::get('chatter.headline') }}</h1>
			<p>{{ Config::get('chatter.description') }}</p>
		@endif
	</div>
<!--END Banner-->


	@if(Session::has('chatter_alert'))
		<div class="chatter-alert alert alert-{{ Session::get('chatter_alert_type') }}">
			<div class="container">
	        	<strong><i class="chatter-alert-{{ Session::get('chatter_alert_type') }}"></i> {{ Config::get('chatter.alert_messages.' . Session::get('chatter_alert_type')) }}</strong>
	        	{{ Session::get('chatter_alert') }}
	        	<i class="chatter-close"></i>
	        </div>
	    </div>
	    <div class="chatter-alert-spacer"></div>
	@endif

	@if (count($errors) > 0)
	    <div class="chatter-alert alert alert-danger">
	    	<div class="container">
	    		<p><strong><i class="chatter-alert-danger"></i> {{ Config::get('chatter.alert_messages.danger') }}</strong> Please fix the following errors:</p>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
	    </div>
	@endif

	<div class="container chatter_container">

	    <div class="row">

	    	<div class="col-md-3 left-column">
	    		<!-- SIDEBAR -->
	    		<div class="chatter_sidebar">
				
				<a href="{{url('/')}}" ><span class='glyphicon glyphicon-home'></span> Home</a>
					
				</div>
				<!-- END SIDEBAR -->
			</div>
	    
	        <div class="col-md-9 right-column">
			<h1 style="padding:5px; background:#fafafa; text-align:center;">Concoeuronline Announcement Board</h1>
	        <div class="panel">
				@forelse($announcement as $announce)
		        	<ul class="discussions" style="padding:5px; margin:5px; background:#fafafa; text-align:justify;">
		        			<div class="panel">
				        	<li style="padding:5px">
							
							<div class="chatter_middle">
				        	<h3 class="chatter_middle_title">{{$announce->title}}</h3>	
				        	<p>{{$announce->body}}</p>	
				        	<p><span>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($announce->created_at))->diffForHumans() }}</span></p>	

							</div>
					        </li>	
							@empty
                    <p colspan="4"><center class="alert alert-danger">No Announcements to display :(</center></p>						
			        </div>	
		        	</ul>
	        	</div>
			@endforelse
	        	<div id="pagination">
	        		{{ $announcement->links() }}
	        	</div>

	        </div>
	    </div>
	</div>

	

@stop
