-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2017 at 07:06 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Balmun Dazang', 'dazangbalmun@live.com', '$2y$10$yiE00aymr4/.kbIxq8vSC.MVw6ACcSpIZFDV.p9dL1QTT2byOul3m', 1, 'vvBlUv1jD38pWv61GFle8amwYNxTTgPhswrIjeo2rrxoLLLqNyKLqHCYiAo7', '2017-12-16 14:16:58', '2017-12-20 12:08:08'),
(2, 'SiteManager', 'admin@admin.com', '$2y$10$VJRAb3MCalDXAm.eocM4AOjoVKxliglJ2FX26I/s91dFao/CSK3we', 1, 'Kcd5QMTpLas14CbfMYjNq64FLThuBGuHr5Wr2xQUmLtskhE0wpcorCIwdtq5', '2017-12-19 16:33:40', '2017-12-20 12:08:36'),
(3, 'Williams Mfon', 'mfonwilliams@gmail.com', '$2y$10$pQGF1B6Y09u7im7AgJAuy.0yt2624un9MHerRToeyleQ5MDCtIcry', 2, 'elaBIZ1UUbekafuQYvxwzxDQKg5myLAeJxi5bxSNJqaobHI54TVbLvtTjJNG', '2017-12-20 12:11:30', '2017-12-20 12:11:30');

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Super', '2017-12-15 14:53:28', NULL),
(2, 'Regular', '2017-12-15 14:53:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(10) UNSIGNED NOT NULL,
  `advert_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advert_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `advertisements`
--

INSERT INTO `advertisements` (`id`, `advert_name`, `advert_image`, `created_at`, `updated_at`) VALUES
(2, 'Asis', '/advertisement/tTwJ4LRA.jpg', '2017-12-19 02:48:20', NULL),
(3, 'gillette', '/advertisement/BcEM6PTA.jpg', '2017-12-19 14:46:11', NULL),
(4, 'Peak Milk', '/advertisement/AvTw31PM.jpg', '2017-12-19 14:55:04', NULL),
(5, 'Peak Banner', '/advertisement/CrE9IMOI.jpg', '2017-12-19 14:55:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(1, 'Breaking News!', 'BALMUN DAZANG has just made it to forbes 100 list of billionairs!!', '2017-12-18 14:10:36', NULL),
(2, 'Spiderman  and Green Goblet', 'The Green Goblet waylaid spiderman on his way to save the city.', '2017-12-18 20:27:36', NULL),
(3, 'PortHarcourt Lady!!', 'Lady declares herself the Commissioner for Food and Drinks, shares official portrait...lol', '2017-12-19 15:37:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chatter_categories`
--

CREATE TABLE `chatter_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chatter_categories`
--

INSERT INTO `chatter_categories` (`id`, `parent_id`, `order`, `name`, `color`, `slug`, `created_at`, `updated_at`) VALUES
(5, NULL, 1, 'Fast Moving Consumer Goods', '#3498DB', 'fmcg', NULL, NULL),
(6, NULL, 2, 'Oil and Gas', '#2ECC71', 'oil_and_gas', NULL, NULL),
(7, NULL, 3, 'Financial Services', '#9B59B6', 'financial_services', NULL, NULL),
(8, NULL, 4, 'Information Technology', '#E67E22', 'information_technology', NULL, NULL),
(9, NULL, 1, 'Media', '#3498DB', 'media', NULL, NULL),
(10, NULL, 2, 'Telecommunications', '#2ECC71', 'telecommunications', NULL, NULL),
(11, NULL, 3, 'Consulting', '#9B59B6', 'consulting', NULL, NULL),
(12, NULL, 4, 'Non-Profit', '#E67E22', 'non_profit', NULL, NULL),
(13, NULL, 1, 'Construction', '#3498DB', 'construction', NULL, NULL),
(14, NULL, 2, 'Pharmaceuticals', '#2ECC71', 'pharmaceuticals', NULL, NULL),
(15, NULL, 3, 'Hospitality/Tourism', '#9B59B6', 'hospitality_tourism', NULL, NULL),
(16, NULL, 4, 'Electricity/Energy', '#E67E22', 'information_technology', NULL, NULL),
(17, NULL, 1, 'Aviation', '#3498DB', 'aviation', NULL, NULL),
(18, NULL, 2, 'Agriculture', '#2ECC71', 'agriculture', NULL, NULL),
(19, NULL, 3, 'Government', '#9B59B6', 'government', NULL, NULL),
(20, NULL, 4, 'Insurance', '#E67E22', 'insurance', NULL, NULL),
(21, NULL, 1, 'Transportation', '#3498DB', 'transportation', NULL, NULL),
(22, NULL, 2, 'Advertising', '#2ECC71', 'advertising', NULL, NULL),
(23, NULL, 3, 'Beauty', '#9B59B6', 'beauty', NULL, NULL),
(24, NULL, 4, 'Health Care', '#E67E22', 'health_care', NULL, NULL),
(25, NULL, 1, 'Fashion', '#3498DB', 'fashion', NULL, NULL),
(26, NULL, 2, 'Education', '#2ECC71', 'education', NULL, NULL),
(27, NULL, 3, 'Food Processing', '#9B59B6', 'food_processing', NULL, NULL),
(28, NULL, 4, 'Waste Disposal', '#E67E22', 'waste_disposal', NULL, NULL),
(29, NULL, 1, 'Franchising', '#3498DB', 'franchising', NULL, NULL),
(30, NULL, 2, 'Wholesale/Retail Sales', '#2ECC71', 'wholesale_retail_sales', NULL, NULL),
(31, NULL, 3, 'Waste Management', '#9B59B6', 'waste_mgt', NULL, NULL),
(32, NULL, 4, 'Mass Media', '#E67E22', 'mass_media', NULL, NULL),
(33, NULL, 1, 'Conulting', '#3498DB', 'consulting', NULL, NULL),
(34, NULL, 2, 'Legal Services', '#2ECC71', 'legal_services', NULL, NULL),
(35, NULL, 3, 'Public Health', '#9B59B6', 'public_health', NULL, NULL),
(36, NULL, 4, 'Gambling', '#E67E22', 'gambling', NULL, NULL),
(37, NULL, 1, 'Mining', '#3498DB', 'mining', NULL, NULL),
(38, NULL, 2, 'Overseas Remittances/Forex', '#2ECC71', 'overseas_remittances_forex', NULL, NULL),
(39, 1, 3, 'Haulage/Logostics', '#9B59B6', 'haulage_logistics', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chatter_discussion`
--

CREATE TABLE `chatter_discussion` (
  `id` int(10) UNSIGNED NOT NULL,
  `chatter_category_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sticky` tinyint(1) NOT NULL DEFAULT '0',
  `views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `answered` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '#232629'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chatter_discussion`
--

INSERT INTO `chatter_discussion` (`id`, `chatter_category_id`, `title`, `user_id`, `sticky`, `views`, `answered`, `created_at`, `updated_at`, `slug`, `color`) VALUES
(2, 5, 'Hello', 1, 0, 0, 0, '2017-12-12 08:37:57', '2017-12-12 08:37:57', 'hello', NULL),
(3, 8, 'GeoTagging', 2, 0, 0, 0, '2017-12-18 18:48:50', '2017-12-18 18:48:50', 'geotagging', '#f04507'),
(4, 9, 'Missing Room Key', 3, 0, 0, 0, '2017-12-21 23:02:17', '2017-12-21 23:02:17', 'missing-room-key', NULL),
(5, 5, 'FindAm Mobile Application', 3, 0, 0, 0, '2017-12-21 23:12:28', '2017-12-21 23:12:28', 'findam-mobile-application', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `chatter_post`
--

CREATE TABLE `chatter_post` (
  `id` int(10) UNSIGNED NOT NULL,
  `chatter_discussion_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `markdown` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chatter_post`
--

INSERT INTO `chatter_post` (`id`, `chatter_discussion_id`, `user_id`, `body`, `created_at`, `updated_at`, `markdown`, `locked`) VALUES
(2, 2, 1, '<p>tbghrbhgbrbghrhghrhghrbghbrhbgrgr</p>', '2017-12-12 08:37:57', '2017-12-12 08:37:57', 0, 0),
(3, 2, 1, '<p><img src=\"https://cnet4.cbsistatic.com/img/QJcTT2ab-sYWwOGrxJc0MXSt3UI=/2011/10/27/a66dfbb7-fdc7-11e2-8c7c-d4ae52e62bcc/android-wallpaper5_2560x1600_1.jpg\" alt=\"\" width=\"80\" height=\"80\" />hfhfhfhfhfhfhfhfhfhfhbvhrbbgverger</p>', '2017-12-12 15:50:01', '2017-12-12 15:50:01', 0, 0),
(4, 2, 1, '<blockquote style=\"text-align: center;\"><ul><li><strong><em><br></em></strong></li><li><strong><em>fd bl tb</em></strong></li><li><strong><em>rbltmr</em></strong></li><li><strong><em>bre</em></strong></li><li><strong><em>ffffff</em></strong><br></li></ul></blockquote>', '2017-12-13 07:20:35', '2017-12-13 07:20:35', 0, 0),
(5, 3, 2, 'Technology is the bane of the 21st centuary!!', '2017-12-18 18:48:50', '2017-12-18 18:48:50', 0, 0),
(6, 4, 3, 'I cant seem to find my house key So i\'ll be sleeping out tonite.', '2017-12-21 23:02:17', '2017-12-21 23:02:17', 0, 0),
(7, 5, 3, 'Try to find it?? What ever your looking for think it!!! We find it!!', '2017-12-21 23:12:29', '2017-12-21 23:12:29', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `chatter_user_discussion`
--

CREATE TABLE `chatter_user_discussion` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `discussion_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `chatter_user_discussion`
--

INSERT INTO `chatter_user_discussion` (`user_id`, `discussion_id`) VALUES
(1, 2),
(2, 3),
(3, 4),
(3, 5);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_07_29_171118_create_chatter_categories_table', 1),
(4, '2016_07_29_171118_create_chatter_discussion_table', 1),
(5, '2016_07_29_171118_create_chatter_post_table', 1),
(6, '2016_07_29_171128_create_foreign_keys', 1),
(7, '2016_08_02_183143_add_slug_field_for_discussions', 1),
(8, '2016_08_03_121747_add_color_row_to_chatter_discussions', 1),
(9, '2017_01_16_121747_add_markdown_and_lock_to_chatter_posts', 1),
(10, '2017_01_16_121747_create_chatter_user_discussion_pivot_table', 1),
(11, '2017_12_15_083757_create_announcement_table', 2),
(12, '2017_12_15_084252_create_advertisement_table', 3),
(13, '2017_12_15_084438_create_report_table', 4),
(14, '2017_12_15_084617_create_tags_table', 5),
(15, '2017_12_15_084959_create_post_tags_table', 6),
(16, '2017_12_15_085328_create_admin_roles_table', 7),
(17, '2017_12_15_085617_create_admin_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `post_tags`
--

CREATE TABLE `post_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tags`
--

INSERT INTO `post_tags` (`id`, `tag_id`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '2017-12-21 23:02:43', NULL),
(2, 1, 14, '2017-12-21 23:02:43', NULL),
(3, 1, 27, '2017-12-21 23:02:43', NULL),
(4, 1, 18, '2017-12-21 23:02:43', NULL),
(5, 1, 28, '2017-12-21 23:04:32', NULL),
(6, 2, 7, '2017-12-21 23:04:32', NULL),
(7, 2, 12, '2017-12-21 23:05:15', NULL),
(8, 2, 20, '2017-12-21 23:05:15', NULL),
(9, 3, 7, '2017-12-21 23:06:10', NULL),
(10, 3, 11, '2017-12-21 23:06:10', NULL),
(11, 3, 13, '2017-12-21 23:06:33', NULL),
(12, 3, 19, '2017-12-21 23:06:33', NULL),
(13, 4, 8, '2017-12-21 23:07:09', NULL),
(14, 4, 9, '2017-12-21 23:07:09', NULL),
(15, 4, 10, '2017-12-21 23:07:25', NULL),
(16, 5, 1, '2017-12-21 23:07:25', NULL),
(17, 5, 16, '2017-12-21 23:07:50', NULL),
(18, 5, 27, '2017-12-21 23:07:50', NULL),
(19, 5, 28, '2017-12-21 23:08:20', NULL),
(20, 6, 9, '2017-12-21 23:08:20', NULL),
(21, 6, 15, '2017-12-21 23:08:43', NULL),
(22, 6, 19, '2017-12-21 23:08:43', NULL),
(23, 6, 22, '2017-12-21 23:09:01', NULL),
(24, 6, 28, '2017-12-21 23:09:01', NULL),
(25, 7, 18, '2017-12-21 23:10:31', NULL),
(26, 7, 27, '2017-12-21 23:10:31', NULL),
(27, 8, 6, '2017-12-21 23:11:22', NULL),
(28, 8, 28, '2017-12-21 23:11:22', NULL),
(29, 8, 29, '2017-12-21 23:11:44', NULL),
(32, 9, 17, '2017-12-21 23:13:07', NULL),
(33, 9, 21, '2017-12-21 23:13:07', NULL),
(34, 10, 1, '2017-12-21 23:13:24', NULL),
(35, 10, 3, '2017-12-21 23:13:24', NULL),
(36, 11, 16, '2017-12-21 23:14:26', NULL),
(37, 11, 19, '2017-12-21 23:14:26', NULL),
(38, 11, 22, '2017-12-21 23:15:10', NULL),
(39, 11, 28, '2017-12-21 23:15:10', NULL),
(40, 12, 10, '2017-12-21 23:16:47', NULL),
(41, 12, 22, '2017-12-21 23:16:47', NULL),
(42, 13, 19, '2017-12-21 23:17:33', NULL),
(43, 13, 26, '2017-12-21 23:17:33', NULL),
(44, 14, 1, '2017-12-21 23:18:29', NULL),
(45, 14, 11, '2017-12-21 23:18:29', NULL),
(46, 14, 16, '2017-12-21 23:19:08', NULL),
(47, 14, 29, '2017-12-21 23:19:08', NULL),
(48, 15, 15, '2017-12-21 23:20:12', NULL),
(49, 16, 11, '2017-12-21 23:20:12', NULL),
(50, 16, 13, '2017-12-21 23:20:46', NULL),
(51, 17, 14, '2017-12-21 23:20:46', NULL),
(52, 17, 24, '2017-12-21 23:21:46', NULL),
(53, 18, 23, '2017-12-21 23:21:46', NULL),
(54, 18, 25, '2017-12-21 23:22:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `title`, `body`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Inappropriate Topic', 'Not suitable for under age users', 'dazangbalmun@live.com', '2017-12-15 11:43:22', NULL),
(2, 'Testing report problem', 'I want to report inappropriate use of language here. It is offensive.', 'tester@tester.com', '2017-12-19 05:29:37', '2017-12-19 05:29:37');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag_name`, `created_at`, `updated_at`) VALUES
(1, 'manufacturing', '2017-12-21 22:11:41', NULL),
(2, 'financial_services', '2017-12-21 22:11:41', NULL),
(3, 'professional_services', '2017-12-21 22:12:21', NULL),
(4, 'ict', '2017-12-21 22:12:21', NULL),
(5, 'industrial_goods', '2017-12-21 22:12:57', NULL),
(6, 'media_entertainment', '2017-12-21 22:12:57', NULL),
(7, 'agriculture', '2017-12-21 22:13:20', NULL),
(8, 'oil_gas', '2017-12-21 22:13:20', NULL),
(9, 'transportation', '2017-12-21 22:14:01', NULL),
(10, 'natural_resources', '2017-12-21 22:14:01', NULL),
(11, 'utilities', '2017-12-21 22:15:29', NULL),
(12, 'telecommunications', '2017-12-21 22:15:29', NULL),
(13, 'education_training', '2017-12-21 22:16:53', NULL),
(14, 'commercial_retail_trade', '2017-12-21 22:16:53', NULL),
(15, 'tourism_hospitality', '2017-12-21 22:20:46', NULL),
(16, 'construction_real_estate', '2017-12-21 22:20:46', NULL),
(17, 'health_care', '2017-12-21 22:21:26', NULL),
(18, 'fashion_beauty', '2017-12-21 22:21:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ifeanyi Osinakayah', 'ifeanyiosinakayah15@gmail.com', '$2y$10$lhHI2MYm2xNTt6OKyv1s/ev2zki7uLVOTVSEJRzYRF7L9DMQdi7Li', NULL, '2017-12-12 07:53:02', '2017-12-12 07:53:02'),
(2, 'Balmun', 'dazangbalmun@gmail.com', '$2y$10$Vruso6mlK/EJ4TRbb2jRGe/kfC2YITzF8eH5qlxfetxRzK1LjZxt6', 'xGdrRmPM9yuUZDpQDUtG6FSINyVku6lPxVSWPIQ2IewjLAzkDeZV4zEbqAKt', '2017-12-15 14:01:38', '2017-12-15 14:01:38'),
(3, 'Idoko', 'idokojudge@gmail.com', '$2y$10$6Lv3086wqMcHRSLU5BESeO7OuaSQ2.8t0jRyRyqsjg3Ij5zooyNLC', 'cXzeC79va9fAnZAEuALwZNg52RAuE2NT5Naqz48OIgwGdqb8DK5B2mMfhNpl', '2017-12-21 02:16:05', '2017-12-21 02:16:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_email_unique` (`email`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatter_categories`
--
ALTER TABLE `chatter_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chatter_discussion`
--
ALTER TABLE `chatter_discussion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `chatter_discussion_slug_unique` (`slug`),
  ADD KEY `chatter_discussion_chatter_category_id_foreign` (`chatter_category_id`),
  ADD KEY `chatter_discussion_user_id_foreign` (`user_id`);

--
-- Indexes for table `chatter_post`
--
ALTER TABLE `chatter_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chatter_post_chatter_discussion_id_foreign` (`chatter_discussion_id`),
  ADD KEY `chatter_post_user_id_foreign` (`user_id`);

--
-- Indexes for table `chatter_user_discussion`
--
ALTER TABLE `chatter_user_discussion`
  ADD PRIMARY KEY (`user_id`,`discussion_id`),
  ADD KEY `chatter_user_discussion_user_id_index` (`user_id`),
  ADD KEY `chatter_user_discussion_discussion_id_index` (`discussion_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `post_tags`
--
ALTER TABLE `post_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `chatter_categories`
--
ALTER TABLE `chatter_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `chatter_discussion`
--
ALTER TABLE `chatter_discussion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `chatter_post`
--
ALTER TABLE `chatter_post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `post_tags`
--
ALTER TABLE `post_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chatter_discussion`
--
ALTER TABLE `chatter_discussion`
  ADD CONSTRAINT `chatter_discussion_chatter_category_id_foreign` FOREIGN KEY (`chatter_category_id`) REFERENCES `chatter_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatter_discussion_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatter_post`
--
ALTER TABLE `chatter_post`
  ADD CONSTRAINT `chatter_post_chatter_discussion_id_foreign` FOREIGN KEY (`chatter_discussion_id`) REFERENCES `chatter_discussion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatter_post_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `chatter_user_discussion`
--
ALTER TABLE `chatter_user_discussion`
  ADD CONSTRAINT `chatter_user_discussion_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `chatter_discussion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chatter_user_discussion_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
