<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    return view('welcome');
    return redirect('forums');
});

//admin routes


Route::get('/admin/logout','AdminController@admin_logout');



//Route::group(['middleware'=>'AuthMiddleWare'],function(){
	
	//Auth::routes();



//announcement routes
Route::get('/admin/announcement','AdminController@announcement');
Route::get('/admin/announcement/create','AdminController@announcement_create');
Route::post('/admin/announcement/create','AdminController@announcement_create_post');
Route::get('/admin/announcement/edit/{id}','AdminController@announcement_edit');
Route::post('/admin/announcement/edit/{id}','AdminController@announcement_edit_post');
Route::get('/admin/announcement/delete/{id}','AdminController@announcement_delete');


//Advertisement routes
Route::get('/admin/advertisement','AdminController@advertisement');
Route::get('/admin/advertisement/create','AdminController@advertisement_create');
Route::post('/admin/advertisement/create','AdminController@advertisement_create_post');
Route::get('/admin/advertisement/edit/{id}','AdminController@advertisement_edit');
Route::post('/admin/advertisement/edit/{id}','AdminController@advertisement_edit_post');
Route::get('/admin/advertisement/delete/{id}','AdminController@advertisement_delete');


//Report routes
Route::get('/admin/report','AdminController@report');



//Admin Users routes

Route::get('/admin/user','AdminController@user');
Route::get('/admin/user/create','AdminController@user_create');
Route::post('/admin/user/create','AdminController@user_create_post');
Route::get('/admin/user/edit/{id}','AdminController@user_edit');
Route::post('/admin/user/edit/{id}','AdminController@user_edit_post');
Route::get('/admin/user/delete/{id}','AdminController@user_delete');

//});


//site routes
	Auth::routes();
	

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/forums/announcement', 'HomeController@announce');
Route::get('/forums/report', 'HomeController@report');
Route::post('/forums/report', 'HomeController@report_form');
Route::post('/forums/report', 'HomeController@report_form');
Route::post('post_image', 'HomeController@postImage')->name('post_image');

Route::prefix('admin')->group(function() {
	
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
   
   Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
   
   Route::get('/dashboard', 'AdminController@dashboard')->name('admin.dashboard');	
//Route::get('/admin/login', 'AdminLoginController@admin_login');
//Route::get('/admin/dashboard','AdminController@dashboard');
});