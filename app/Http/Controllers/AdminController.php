<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\Announcements;
use App\Advertisements;
use App\Admin;

class AdminController extends Controller {
	
	
	
	 /**
     * Create a new controller instance.
     *
     * @return void
     */
	 
	public function __construct()
    {
        $this->middleware('auth:admin');
    }
	
	
   public function admin_logout(){
	
	Auth::logout();
	Session::flush();
    
    return redirect('/admin/login');
   }
	//Admin Users Controller
	public function user() {
		
		$users=Admin::paginate(10);
		return view('admin.users',compact('users'));
	}
	
	public function user_create() {
		
		$roles=DB::table('admin_roles')->get();
		return view('admin.users-create', compact('roles'));
	}
	
	public function user_create_post(Request $request) {
		
		     if($request->password==$request->confirm_password){
				 
            Admin::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'role'=>$request->role,
            ]);
            
            return back()->with('message','<b>Success</b> Admin account created');
			
        }else{
			
            return back()->with('error','<b>Failed</b> Password Mis-match / Password too short min. of 6 characters');
        }

	}
	
	public function user_edit($id) {
		
		$user = DB::table('admins')->where('id',$id)->first();
		$roles=DB::table('admin_roles')->get();
		
		return view('admin.users-update',compact('user','roles'));
	}
	
	public function user_edit_post(Request $request) {
		
		 if($request->password==$request->confirm_password){
		DB::table('admins')->where('id',$request->id)->update([
            
			'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'role'=>$request->role,
            'updated_at'=>Carbon::now()
        ]);
	
	return back()->with('message','Admin Profile Successfully Updated!');
		 } else {
			 
		return back()->with('error','<b>Failed</b> Password Mis-match / Password too short min. of 6 characters');
	}
	
	}
	
	public function user_delete($id) {
		
		DB::table('admins')->where('id',$id)->delete();
		
		return back()->with('message','Administrator Account successfully Deleted!');
	}
	
	//END Admin Users
	
	
	//Dashboard
	public function dashboard()
	{
		$categories = DB::table('chatter_categories')->limit(10)->get();
		$discussions = DB::table('chatter_discussion')->limit(10)->orderBy('created_at', 'desc')->get();
		
		

		$users = DB::table('users')->get();
		//var_dump($posts);exit;
		return view('admin.dashboard', compact('categories','discussions','users'));
	}
	
	
	//END Dashboard
	
	
	//Announcement Controller
	public function announcement(){
		
		$announce = DB::table('announcements')->paginate(10);
		
		return view('admin.announcement', compact('announce'));
		
	}
	
	public function announcement_create(){
		
		return view('admin.announcement-create');
	}
	
	public function announcement_create_post(Request $request){
		
		if($request->title != null && $request->body != null){
				
				Announcements::create([	         
                'title'=>$request->title,        
                'body'=>$request->body,        
			]);
		}
		
		return back()->with('message','Announcements Successfully added!');
	}
	
	public function announcement_edit($id){
		
		$announce = DB::table('announcements')->where('id',$id)->first();
		
		return view('admin.announcement-update', compact('announce'));
	}
	
	public function announcement_edit_post(Request $request){
		
		
		DB::table('announcements')->where('id',$request->id)->update([
            
			'title'=>$request->title,
            'body'=>$request->body,
            'updated_at'=>Carbon::now()
        ]);
	
	return back()->with('message','successful!');
	}
	
	
	public function announcement_delete($id){
		
		DB::table('announcements')->where('id',$id)->delete();
		
		return back()->with('message','Announcement Delete successful!');
	}
	
	//END Announcement
	
	//Advertisement Controller
	public function advertisement(){
		
		$advert = DB::table('advertisements')->paginate(10);
	
		return view('admin.advertisement', compact('advert'));
		
	}
	
	public function advertisement_create(){
		
		return view('admin.advertisement-create');
		
	}
	
	public function advertisement_create_post(Request $request){
		
		if($request->advert_image != null){
            
            $path = str_random(8).".".$request->advert_image->getClientOriginalExtension(); 
            $request->advert_image-> move(public_path('/advertisement'),$path);
			
			Advertisements::create([	 
                'advert_name'=>$request->advert_name,
                'advert_image'=>'/advertisement/'.$path,         
			]);
			
			}
		
		return back()->with('message','Advertisement Successfully added!');
		
	}
	
	public function advertisement_edit($id){
		
		$advert = DB::table('advertisements')->where('id',$id)->first();
		
		return view('admin.advertisement-update', compact('advert'));
		
		
	}
	
	public function advertisement_edit_post(Request $request){
		
		$path = str_random(8).".".$request->advert_image->getClientOriginalExtension(); 
        $request->advert_image-> move(public_path('/advertisement'),$path);
		
		DB::table('advertisements')->where('id',$request->id)->update([
            
			'advert_name'=>$request->advert_name,
            'advert_image'=>'/advertisement/'.$path,
            'updated_at'=>Carbon::now()
        ]);
	
	return back()->with('message','successful!');
		
	}
	
	public function advertisement_delete($id){
		
		DB::table('advertisements')->where('id',$id)->delete();
		
		return back()->with('message','Advertisements Delete successful!!');
		
	}
	
	//END Advertisement 
	
	//Report Controller
	public function report (){
		
		$reports = DB::table('reports')->paginate(20);
		
		return view('admin.report', compact('reports'));
	}
	
	//END Report

}