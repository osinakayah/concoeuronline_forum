<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class AdminLoginController extends Controller
{
    //
	
	use AuthenticatesUsers;

    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }
    /**
     *
     * @return property guard use for login
     *
     */
    public function guard()
    {
     return Auth::guard('admin');
   }
   
   public function admin_login(){
	   
	   return view('admin.login');
   }
   
   public function admin_logout(){
	
	Auth::logout();
    
    return redirect('/admin/login');
   }
}
