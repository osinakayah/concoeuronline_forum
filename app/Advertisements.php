<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisements extends Model
{
    //
	protected $fillable = ['advert_name','advert_image'];
}
