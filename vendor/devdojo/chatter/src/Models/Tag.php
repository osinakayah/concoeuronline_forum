<?php

namespace DevDojo\Chatter\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	 protected $table = 'tags';
   
    public $timestamps = true;
    
	protected $fillable = ['tag_name'];

	public function discus(){
		
		return $this -> belongsToMany(Models::className(Discussion::class),'post_tags','post_id','tag_id');
	}
	
	
    public function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }
}