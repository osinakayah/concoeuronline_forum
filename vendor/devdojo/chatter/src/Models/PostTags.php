<?php

namespace DevDojo\Chatter\Models;

use Illuminate\Database\Eloquent\Model;

class PostTags extends Model
{
	 protected $table = 'post_tags';
   
    public $timestamps = true;
    
	protected $fillable = ['tag_id','post_id'];

	public function discussion(){
		
		return $this -> belongsToMany(Models::className(Discussion::class),'post_tags','post_id','tag_id');
	}
	
	 public function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }
  
  public function discussions()
    {
        return $this->hasMany(Models::className(Discussion::class));
    }
}