@extends(Config::get('chatter.master_file_extend'))

@section(Config::get('chatter.yields.head'))
    <link href="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.css" rel="stylesheet">
	<link href="/vendor/devdojo/chatter/assets/css/chatter.css" rel="stylesheet">
	@if($chatter_editor == 'simplemde')
		<link href="/vendor/devdojo/chatter/assets/css/simplemde.min.css" rel="stylesheet">
	@elseif($chatter_editor == 'trumbowyg')
		<link href="/vendor/devdojo/chatter/assets/vendor/trumbowyg/ui/trumbowyg.css" rel="stylesheet">
		<style>
			.trumbowyg-box, .trumbowyg-editor {
				margin: 0px auto;
			}
		</style>
	@endif
@stop

@section('content')

<div id="chatter" class="chatter_home">
<!--Banner logo-->
	<div id="chatter_hero">
		<div id="chatter_hero_dimmer">
		
		</div>
		<?php $headline_logo = Config::get('chatter.headline_logo'); ?>
		
		@if( isset( $headline_logo ) && !empty( $headline_logo ) )
			
			<img src="{{ Config::get('chatter.headline_logo') }}">
			
		@else
			
			<h1>{{ Config::get('chatter.headline') }}</h1>
			<p>{{ Config::get('chatter.description') }}</p>
		@endif
	</div>
<!--END Banner-->


	@if(Session::has('chatter_alert'))
		<div class="chatter-alert alert alert-{{ Session::get('chatter_alert_type') }}">
			<div class="container">
	        	<strong><i class="chatter-alert-{{ Session::get('chatter_alert_type') }}"></i> {{ Config::get('chatter.alert_messages.' . Session::get('chatter_alert_type')) }}</strong>
	        	{{ Session::get('chatter_alert') }}
	        	<i class="chatter-close"></i>
	        </div>
	    </div>
	    <div class="chatter-alert-spacer"></div>
	@endif

	@if (count($errors) > 0)
	    <div class="chatter-alert alert alert-danger">
	    	<div class="container">
	    		<p><strong><i class="chatter-alert-danger"></i> {{ Config::get('chatter.alert_messages.danger') }}</strong> Please fix the following errors:</p>
		        <ul>
		            @foreach ($errors->all() as $error)
		                <li>{{ $error }}</li>
		            @endforeach
		        </ul>
		    </div>
	    </div>
	@endif

	<div class="container chatter_container">

	    <div class="row">

	    	<div class="col-md-3 left-column">
	    		<!-- SIDEBAR -->
	    		<div class="chatter_sidebar">
					<button class="btn btn-primary" id="new_discussion_btn"><i class="chatter-new"></i> New {{ Config::get('chatter.titles.discussion') }}</button>
					<a href="/{{ Config::get('chatter.routes.home') }}"><i class="chatter-bubble"></i> All {{ Config::get('chatter.titles.discussions') }}</a>
					<a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.announcement') }}"/><i class="chatter-bubble"></i> ConcoeurOnline {{ Config::get('chatter.titles.announcement') }}</a>
					
					<ul class="nav nav-pills nav-stacked">
						
						<?php $categories = DevDojo\Chatter\Models\Models::category()->all(); ?>
						
						@foreach($categories as $category)
						
						<li><a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.category') }}/{{ $category->slug }}"><div class="chatter-box" style="background-color:{{ $category->color }}"></div> {{ $category->name }}</a></li>

						@endforeach
					</ul>
				</div>
				<!-- END SIDEBAR -->
	    	</div>
	        <div class="col-md-6 center-column">
	        	<div class="panel">
		        	<ul class="discussions">
		        		@forelse($discussions as $discussion)
				        	<li>
				        		<a class="discussion_list" href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}/{{ $discussion->category->slug }}/{{ $discussion->slug }}">
					        		<div class="chatter_avatar">
					        			@if(Config::get('chatter.user.avatar_image_database_field'))

					        				<?php $db_field = Config::get('chatter.user.avatar_image_database_field'); ?>

					        				<!-- If the user db field contains http:// or https:// we don't need to use the relative path to the image assets -->
					        				@if( (substr($discussion->user->{$db_field}, 0, 7) == 'http://') || (substr($discussion->user->{$db_field}, 0, 8) == 'https://') )
					        					<img src="{{ $discussion->user->{$db_field}  }}">
					        				@else
					        					<img src="{{ Config::get('chatter.user.relative_url_to_image_assets') . $discussion->user->{$db_field}  }}">
					        				@endif

					        			@else

					        				<span class="chatter_avatar_circle" style="background-color:#<?= \DevDojo\Chatter\Helpers\ChatterHelper::stringToColorCode($discussion->user->email) ?>">
					        					{{ strtoupper(substr($discussion->user->email, 0, 1)) }}
					        				</span>

					        			@endif
					        		</div>

					        		<div class="chatter_middle">
									
					        			<h3 class="chatter_middle_title">{{ $discussion->title }} <div class="chatter_cat" style="background-color:{{ $discussion->category->color }}">{{ $discussion->category->name }}</div></h3>
					        			<span class="chatter_middle_details">Posted By: <span data-href="/user">{{ ucfirst($discussion->user->{Config::get('chatter.user.database_field_with_user_name')}) }}</span> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($discussion->created_at))->diffForHumans() }}</span>
					        			@if($discussion->post[0]->markdown)
					        				<?php $discussion_body = GrahamCampbell\Markdown\Facades\Markdown::convertToHtml( $discussion->post[0]->body ); ?>
					        			@else
					        				<?php $discussion_body = $discussion->post[0]->body; ?>
					        			@endif
					        			<p>{{ substr(strip_tags($discussion_body), 0, 200) }}@if(strlen(strip_tags($discussion_body)) > 200){{ '...' }}@endif</p>
					        		</div>

					        		<div class="chatter_right">

					        			<div class="chatter_count"><i class="chatter-bubble"></i> {{ $discussion->postsCount[0]->total }}</div>
					        		</div>

					        		<div class="chatter_clear"></div>
					        	</a>
				        	</li>
							
							@empty
							<div class="alert alert-danger"><center>No Discussion to display :(</center></div>
                  
			        	@endforelse
		        	</ul>
	        	</div>

	        	<div id="pagination">
	        		{{ $discussions->links() }}
	        	</div>

	        </div>
			
		<div class="col-md-3 right-column" style="margin-top:-21px;">
			<div class="chatter_sidebar">
			<hr>
			 <h3 style="background-color:#fafafa; padding:3px;">Advertisements</h3>
			  <hr>
			  @foreach($advertisements as $advert)
			    @if($advert->advert_image != null)
                            <?php
                                $arr = explode('/', $advert->advert_image);
                                $advert->advert_image = "/".$arr[1]."/".$arr[2];
                            ?>
        
		<img src="{{asset($advert->advert_image)}}" style="width:300px;"  class="img-responsive"><br>
	
		 @endif
		 	 @endforeach
	    </div>
	    </div>
		
	   </div>
	</div>

	<div id="new_discussion">


    	<div class="chatter_loader dark" id="new_discussion_loader">
		    <div></div>
		</div>

    	<form id="chatter_form_editor" action="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}" method="POST">
        	<div class="row">
	        	<div class="col-md-7">
		        	<!-- TITLE -->
	                {{--<input type="text" class="form-control" id="title" name="title" placeholder="Title of {{ Config::get('chatter.titles.discussion') }}" v-model="title" value="{{ old('title') }}" >--}}
					<input type="text" class="form-control" id="title" name="title" placeholder="Title of {{ Config::get('chatter.titles.discussion') }}" value="{{ old('title') }}" >
	            </div>

	            <div class="col-md-4">
		            <!-- CATEGORY -->
			            <select id="chatter_category_id" class="form-control" name="chatter_category_id">
			            	<option value="">Select a Category</option>
				            @foreach($categories as $category)
				            	@if(old('chatter_category_id') == $category->id)
				            		<option value="{{ $category->id }}" selected>{{ $category->name }}</option>
				            	@else
				            		<option value="{{ $category->id }}">{{ $category->name }}</option>
				            	@endif
				            @endforeach
			            </select>
		        </div>

		        <div class="col-md-1">
		        	<i class="chatter-close"></i>
		        </div>
	        </div><!-- .row -->

            <!-- BODY -->
        	<div id="editor">
        		@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
					<label id="tinymce_placeholder">Type Your Discussion Here...</label>
    				<textarea id="body" class="richText" name="body" placeholder="">{{ old('body') }}</textarea>
    			@elseif($chatter_editor == 'simplemde')
    				<textarea id="simplemde" name="body" placeholder="">{{ old('body') }}</textarea>
				@elseif($chatter_editor == 'trumbowyg')
					<textarea class="trumbowyg" name="body" placeholder="Type Your Discussion Here...">{{ old('body') }}</textarea>
				@endif
    		</div>

            <input type="hidden" name="_token" id="csrf_token_field" value="{{ csrf_token() }}">

            <div id="new_discussion_footer">
            	<input type='text' id="color" name="color" /><span class="select_color_text">Select a Color for this Discussion (optional)</span>
            	<button id="submit_discussion" class="btn btn-success pull-right"><i class="chatter-new"></i> Create {{ Config::get('chatter.titles.discussion') }}</button>
            	<a href="/{{ Config::get('chatter.routes.home') }}" class="btn btn-default pull-right" id="cancel_discussion">Cancel</a>
            	<div style="clear:both"></div>
            </div>
        </form>

    </div><!-- #new_discussion -->

</div>

@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
	<input type="hidden" id="chatter_tinymce_toolbar" value="{{ Config::get('chatter.tinymce.toolbar') }}">
	<input type="hidden" id="chatter_tinymce_plugins" value="{{ Config::get('chatter.tinymce.plugins') }}">
@endif
<input type="hidden" id="current_path" value="{{ Request::path() }}">

@endsection

@section(Config::get('chatter.yields.footer'))

 <footer id="footer" class="style4">
 
            <div class="footer-wrapper">
			
                <div class="container">
				
                    <div class="row add-clearfix same-height">
					
                        <div class="col-sm-6 col-md-3">
						
                            <h5 class="section-title box">Recent Posts</h5>
							
                            <ul class="recent-posts">
							@foreach($discuss as $discussion)
                                <li>
									   	<a class="discussion_list" href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.discussion') }}/{{ $discussion->category->slug }}/{{ $discussion->slug }}">
					        	
								   <div class="post-content">
							
                                    <div class="chatter_middle">
									
					        			<h3 class="chatter_middle_title">{{ $discussion->title }} </h3>
										<div class="chatter_cat" style="background-color:{{ $discussion->category->color }}; border-radius:5px; text-align:center; margin-top:-15px; padding-left:5px;"><p>{{ $discussion->category->name }}</p></div>
					        			<span class="chatter_middle_details">Posted By: <span data-href="/user">{{ ucfirst($discussion->user->{Config::get('chatter.user.database_field_with_user_name')}) }}</span> {{ \Carbon\Carbon::createFromTimeStamp(strtotime($discussion->created_at))->diffForHumans() }}</span>
												@if($discussion->post[0]->markdown)
					        				<?php $discussion_body = GrahamCampbell\Markdown\Facades\Markdown::convertToHtml( $discussion->post[0]->body ); ?>
					        			@else
					        				<?php $discussion_body = $discussion->post[0]->body; ?>
					        			@endif
					        			<p>{{ substr(strip_tags($discussion_body), 0, 200) }}@if(strlen(strip_tags($discussion_body)) > 200){{ '...' }}@endif</p>
									</div>
					        			
                                    </div>
								</a>	
                                </li>
								
								@endforeach
                            </ul>
							
						</div>
						
                        <div class="col-sm-9 col-md-6">
						
                            <h5 class="section-title box">Popular Tags</h5>
							
                            <div class="tags">
							
								
							@forelse ($tags as $taggin)
							
							
							
								@if($taggin->id==1)
                                <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->id}}" class="tag">manufacturing</a>
								
									@elseif ($taggin->id==2)
								<a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Financial Services</a>
									@elseif($taggin->id==3)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Professional Services </a>
									@elseif ($taggin->id==4)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">ICT</a>
									@elseif($taggin->id==5)
							  <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Industrial Goods</a>
									@elseif	($taggin->id==6)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Media/Entertainment</a>
									@elseif	($taggin->id==7)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Agriculture</a>
									@elseif	($taggin->id==8)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Oil & Gas</a>
									@elseif	($taggin->id==9)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Transportation</a>
									@elseif	($taggin->id==10)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Natural Resources</a>    
									@elseif	 ($taggin->id==11)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Utilities</a>
									@elseif	  ($taggin->id==12)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Telecommunications</a>
									@elseif	  ($taggin->id==13)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Education & Training</a>
									@elseif	   ($taggin->id==14)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Commercial/Retail Trade</a>
									@elseif		($taggin->id==15)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Tourism/Hospitality</a>
										@elseif	($taggin->id==16)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Construction/Real Estate</a>
										@elseif	($taggin->id==17)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Health Care</a>
										@elseif	($taggin->id==18)
							   <a href="/{{ Config::get('chatter.routes.home') }}/{{ Config::get('chatter.routes.tag') }}/{{$taggin->tag_name}}" class="tag">Fashion & Beauty</a>         

								@endif
								
								@empty
								
								@endforelse
								
                            </div>
							
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <h5 class="section-title box">About ConcoeurOnline</h5>
                            <p>ConcoeurOnline is a Brand well thought out to deliver - Responsive - Multi-Purpose & Engaging Premium Online Forum which sets new standards for the discussions in 2017.</p>
                           
						   <div class="social-icons">
						   
                                <a href="www.twitter.com" class="social-icon"><i class="fa fa-twitter has-circle" data-toggle="tooltip" data-placement="top" title="Twitter"></i></a>
                                <a href="www.facebook.com" class="social-icon"><i class="fa fa-facebook has-circle" data-toggle="tooltip" data-placement="top" title="Facebook"></i></a>
                                <a href="plus.google.com" class="social-icon"><i class="fa fa-google-plus has-circle" data-toggle="tooltip" data-placement="top" title="GooglePlus"></i></a>
                                <a href="www.instagram.com" class="social-icon"><i class="fa fa-instagram has-circle" data-toggle="tooltip" data-placement="top" title="Instagram"></i></a>

                            </div>
							
                            <a href="#" class="btn btn-sm style4">Contact Us</a>
                            <a href="#" class="back-to-top"><span></span></a>
                        </div>
                    </div>
                </div>
            </div>
			
            <div class="footer-bottom-area">
			
                <div class="container">
				
                    <div class="copyright-area">
					
                        <div class="copyright">
						
                            &copy; {{date('Y')}} ConcoeurOnline
                        </div>
						
                    </div>
					
                </div>
				
            </div>
			
        </footer>
    <!--End Footer-->
	
	



@if( $chatter_editor == 'tinymce' || empty($chatter_editor) )
	<script src="/vendor/devdojo/chatter/assets/vendor/tinymce/tinymce.min.js"></script>
	<script src="/vendor/devdojo/chatter/assets/js/tinymce.js"></script>
	<script>
		var my_tinymce = tinyMCE;
		$('document').ready(function(){
			$('#tinymce_placeholder').click(function(){
				my_tinymce.activeEditor.focus();
			});
		});
	</script>
@elseif($chatter_editor == 'simplemde')
	<script src="/vendor/devdojo/chatter/assets/js/simplemde.min.js"></script>
	<script src="/vendor/devdojo/chatter/assets/js/chatter_simplemde.js"></script>
@elseif($chatter_editor == 'trumbowyg')
	<script src="/vendor/devdojo/chatter/assets/vendor/trumbowyg/trumbowyg.min.js"></script>
	<script src="/vendor/devdojo/chatter/assets/vendor/trumbowyg/plugins/preformatted/trumbowyg.preformatted.min.js"></script>
	<script src="/vendor/devdojo/chatter/assets/js/trumbowyg.js"></script>
@endif

<script src="/vendor/devdojo/chatter/assets/vendor/spectrum/spectrum.js"></script>
<script src="/vendor/devdojo/chatter/assets/js/chatter.js"></script>
<script>
	$('document').ready(function(){

		$('.chatter-close').click(function(){
			$('#new_discussion').slideUp();
		});
		$('#new_discussion_btn, #cancel_discussion').click(function(){
			@if(Auth::guest())
				window.location.href = "/{{ Config::get('chatter.routes.home') }}/login";
			@else
				$('#new_discussion').slideDown();
				$('#title').focus();
			@endif
		});

		$("#color").spectrum({
		    color: "#333639",
		    preferredFormat: "hex",
		    containerClassName: 'chatter-color-picker',
		    cancelText: '',
    		chooseText: 'close',
		    move: function(color) {
				$("#color").val(color.toHexString());
			}
		});

		@if (count($errors) > 0)
			$('#new_discussion').slideDown();
			$('#title').focus();
		@endif


	});
</script>

	<!-- Javascript -->
    <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.8.3.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.11.2.min.js"></script>

	
    <!-- load page Javascript -->
    <script type="text/javascript" src="js/main.js"></script>
	
<!-- Magnific Popup core JS file -->
    <script type="text/javascript" src="components/magnific-popup/jquery.magnific-popup.min.js"></script> 
    
    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>


    <!-- plugins -->
    <script type="text/javascript" src="js/jquery.plugins.js"></script>
@stop

