<?php

namespace DevDojo\Chatter\Controllers;

use Auth;
use DevDojo\Chatter\Models\Models;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as Controller;
use DB;

class ChatterController extends Controller
{
    public function search(Request $request)
    {
        $pagination_results = config('chatter.paginate.num_of_results');

        $discussions = Models::discussion()->where('title', 'like', '%'.$request->get('q', '').'%')->with('user')->with('post')->with('postsCount')->with('category')->orderBy('created_at', 'DESC')->paginate($pagination_results);
        if (isset($slug)) {
            $category = Models::category()->where('slug', '=', $slug)->first();
            if (isset($category->id)) {
                $discussions = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->where('chatter_category_id', '=', $category->id)->orderBy('created_at', 'DESC')->paginate($pagination_results);
            }
        } 

		$discuss = Models::discussion()->where('title', 'like', '%'.$request->get('q', '').'%')->with('user')->with('post')->with('postsCount')->with('category')->orderBy('created_at', 'DESC')->limit(3)->get();
        if (isset($slug)) {
            $category = Models::category()->where('slug', '=', $slug)->first();
            if (isset($category->id)) {
                $discuss = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->where('chatter_category_id', '=', $category->id)->orderBy('created_at', 'DESC')->limit(3)->get();
            }
        }

        $categories = Models::category()->all();
        $chatter_editor = config('chatter.editor');

        if ($chatter_editor == 'simplemde') {
            // Dynamically register markdown service provider
            \App::register('GrahamCampbell\Markdown\MarkdownServiceProvider');
        }

		$advertisements = DB::table('advertisements')->limit(5)->orderBy('id','desc')->get();
		
        return view('chatter::home', compact('discussions', 'categories', 'chatter_editor','advertisements','discuss'));
    }
    public function index($slug = null)
    {
		//Pagination
        $pagination_results = config('chatter.paginate.num_of_results');

		//Content
        $discussions = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->orderBy('created_at', 'DESC')->paginate($pagination_results);
        if (isset($slug)) {
            $category = Models::category()->where('slug', '=', $slug)->first();
            if (isset($category->id)) {
                $discussions = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->where('chatter_category_id', '=', $category->id)->orderBy('created_at', 'DESC')->paginate($pagination_results);
            }
        } 
		
		//Recent Post
		$discuss = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->orderBy('created_at', 'DESC')->limit(3)->get();
        if (isset($slug)) {
            $category = Models::category()->where('slug', '=', $slug)->first();
            if (isset($category->id)) {
                $discuss = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->where('chatter_category_id', '=', $category->id)->orderBy('created_at', 'DESC')->limit(3)->get();
            }
        }

		//All Categories
        $categories = Models::category()->all();
		
		//All Tags
        $tags = Models::tag()->all();
		
		//Editor
        $chatter_editor = config('chatter.editor');

        if ($chatter_editor == 'simplemde') {
            // Dynamically register markdown service provider
            \App::register('GrahamCampbell\Markdown\MarkdownServiceProvider');
        }
		$advertisements = DB::table('advertisements')->limit(5)->orderBy('id','desc')->get();

        return view('chatter::home', compact('discussions', 'categories', 'chatter_editor','advertisements','discuss','tags'));
    }

    public function login()
    {
        if (!Auth::check()) {
            return \Redirect::to('/'.config('chatter.routes.login').'?redirect='.config('chatter.routes.home'))->with('flash_message', 'Please create an account before posting.');
        }
    }

    public function register()
    {
        if (!Auth::check()) {
            return \Redirect::to('/'.config('chatter.routes.register').'?redirect='.config('chatter.routes.home'))->with('flash_message', 'Please register for an account.');
        }
    }
	
	public function show($slug=null){
		 $pagination_results = config('chatter.paginate.num_of_results');
		 
		 //Tags
		 
		 $post_tags = Models::post_tags()->all();
		 
    
		 
		 
		  //Tags
		/*  $discussions = Models::post_tags()->with('user')->with('post')->with('postsCount')->with('category')->with(discuss)->orderBy('created_at', 'DESC')->paginate($pagination_results);
		 
		  if (isseisset($slug)) {
            $tag_category = Models::tag()->where('tag_name', '=', $tag)->first();
            if (isset($tag_category->id)) {
                $discussions = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->where('chatter_category_id', '=', $category->id)->orderBy('created_at', 'DESC')->paginate($pagination_results);
            }
        } */
		 
		 
		 //Recent Post
		$discuss = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->orderBy('created_at', 'DESC')->limit(3)->get();
      
	  if (isset($slug) ) {
			
            $category = Models::category()->where('slug', '=', $slug)->first();
			
            if (isset($category->id)) {
				
                $discuss = Models::discussion()->with('user')->with('post')->with('postsCount')->with('category')->where('chatter_category_id', '=', $category->id)->orderBy('created_at', 'DESC')->limit(3)->get();
            }
        }

		//All Categories
      $categories = Models::category()->all();
	  
		//All Tags
      $tags = Models::tag()->all();
		
		//Editor
        $chatter_editor = config('chatter.editor');

        if ($chatter_editor == 'simplemde') {
            // Dynamically register markdown service provider
            \App::register('GrahamCampbell\Markdown\MarkdownServiceProvider');
        }
		$advertisements = DB::table('advertisements')->limit(5)->orderBy('id','desc')->get();
		
		
		/*if($categories->id == $post_tags->post_id){
			
			$discussions = Models::post_tags()->with('user')->with('post')->with('postsCount')->with('category')->with(discuss)->orderBy('created_at', 'DESC')->paginate($pagination_results);
		 
			
		}*/

		return view('chatter::tag',compact('discussions','categories', 'chatter_editor','advertisements','discuss','tags'));
	}
}
